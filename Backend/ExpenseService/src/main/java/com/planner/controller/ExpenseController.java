package com.planner.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.planner.entity.Expense;
import com.planner.entity.ExpenseDTO;
import com.planner.exception.ObjectNotFoundException;
import com.planner.service.ExpenseService;


@RestController
@RequestMapping("/expense")
public class ExpenseController {
	
	@Autowired
	private ExpenseService expenseService;
	
	@GetMapping("/getById/{id}")
	public ResponseEntity<ExpenseDTO> getById(@PathVariable long id) throws ObjectNotFoundException{
		ExpenseDTO exp1 = expenseService.getById(id);
		if (exp1 !=null) {
			return ResponseEntity.ok(exp1);
		} else {
			throw new ObjectNotFoundException("The object with given ID is not present!");
		}
	}
	
	@PostMapping
	public ResponseEntity<ExpenseDTO> addExpense(@RequestBody Expense expense) {
		ExpenseDTO newExpense= expenseService.addExpense(expense);
		return new ResponseEntity<>(newExpense, HttpStatus.CREATED);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ExpenseDTO> updateExpense(@PathVariable("id") long id,@RequestBody Expense expense){
		ExpenseDTO updated = expenseService.updateExpense(id, expense);
		if (updated != null) {
			return ResponseEntity.status(HttpStatus.OK).build();
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}
	
	@DeleteMapping("/{id}")
	public String deleteExpense(@PathVariable long id) {
		return expenseService.deleteExpense(id);
	}
	
	@GetMapping("/getByUserId/{id}")
	public ResponseEntity<List<ExpenseDTO>> getByUserId(@PathVariable long id){
		return ResponseEntity.ok(expenseService.getByUser(id));
	}
	
}
