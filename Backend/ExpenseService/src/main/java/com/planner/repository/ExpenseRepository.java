package com.planner.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.planner.entity.Expense;

public interface ExpenseRepository extends JpaRepository<Expense, Long>{

	List<Expense> findAllByUserId(long id);

}
