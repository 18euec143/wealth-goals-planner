package com.planner.entity;

import com.planner.enums.ExpenseCategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExpenseDTO {
	private long expId;
	private ExpenseCategory category;
	private String description;
	private long userId;
}
