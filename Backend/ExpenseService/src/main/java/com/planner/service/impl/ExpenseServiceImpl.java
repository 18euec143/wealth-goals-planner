package com.planner.service.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.planner.entity.Expense;
import com.planner.entity.ExpenseDTO;
import com.planner.repository.ExpenseRepository;
import com.planner.service.ExpenseService;

@Service
public class ExpenseServiceImpl implements ExpenseService{
	@Autowired
	ModelMapper modelMapper;
	
	@Autowired
	private ExpenseRepository expenseRepository;

	@Override
	public ExpenseDTO getById(long id) {
		Optional<Expense> exp = expenseRepository.findById(id);
		if(exp.isPresent()) {
			return modelMapper.map(exp.get(), ExpenseDTO.class);
		}
		return null;
	}

	@Override
	public ExpenseDTO addExpense(Expense expense) {
		Expense exp = expenseRepository.save(expense);
		return modelMapper.map(exp, ExpenseDTO.class);
	}

	@Override
	public ExpenseDTO updateExpense(long id, Expense changedExpense) {
		Optional<Expense> existingExpense = expenseRepository.findById(id);
        if (existingExpense.isPresent()) {
        	Expense updatedObj = existingExpense.get();
        	updatedObj.setCategory(changedExpense.getCategory());
        	updatedObj.setDescription(changedExpense.getDescription());
        	expenseRepository.save(updatedObj);
        }
		return modelMapper.map(changedExpense, ExpenseDTO.class);
	}

	@Override
	public String deleteExpense(long id) {
		Optional<Expense> exp = expenseRepository.findById(id);
		if (exp.isPresent()) {
			expenseRepository.deleteById(id);
			return "Successfully deleted!";
		}
		return "Failed to delete";
	}

	@Override
	public List<ExpenseDTO> getByUser(long id) {
		List<Expense> listByUser = expenseRepository.findAllByUserId(id);
		List<ExpenseDTO> expenseDTOS = listByUser.stream().map(el -> modelMapper.map(el, ExpenseDTO.class)).toList();
		return expenseDTOS;
	}
	

}
