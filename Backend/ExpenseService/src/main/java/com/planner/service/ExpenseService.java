package com.planner.service;

import java.util.List;
import java.util.Optional;

import com.planner.entity.Expense;
import com.planner.entity.ExpenseDTO;

public interface ExpenseService {

	ExpenseDTO getById(long id);

	ExpenseDTO addExpense(Expense expense);

	ExpenseDTO updateExpense(long id, Expense expense);

	String deleteExpense(long id);

	List<ExpenseDTO> getByUser(long id);

	
}
