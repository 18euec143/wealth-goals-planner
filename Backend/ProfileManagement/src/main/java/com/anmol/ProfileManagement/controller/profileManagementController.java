package com.anmol.ProfileManagement.controller;

import com.anmol.ProfileManagement.DTO.GoalDto;
import com.anmol.ProfileManagement.DTO.SubordinateDTO;
import com.anmol.ProfileManagement.DTO.UserInfoDTO;
import com.anmol.ProfileManagement.extern.GoalServiceFeignClient;
import com.anmol.ProfileManagement.extern.SubordinateServiceFeignClient;
import com.anmol.ProfileManagement.extern.UserServiceFeignClient;
import com.anmol.ProfileManagement.model.Profile;
import com.anmol.ProfileManagement.service.ProfileService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/profile")
public class profileManagementController {

    @Autowired
    ProfileService profileService;

    @GetMapping("/{id}")
    @CircuitBreaker(name = "PROFILE-BY-END-USER-ID",fallbackMethod = "getProfileByEndUserIdFallback")
    public ResponseEntity<Profile> getProfileByEndUserId(@PathVariable("id") Long endUserId){
        Profile profileByUserId = profileService.getProfileByUserId(endUserId);
        return  ResponseEntity.ok(profileByUserId);
    }


    public ResponseEntity<Profile> getProfileByEndUserIdFallback(@PathVariable("id") Long endUserId,Exception ex){
        Profile profile = Profile.builder()
                .goals(List.of()).expenses(List.of()).investments(List.of()).subordinates(List.of())
                .userInfo(null)
                .build();
        return  ResponseEntity.ok(profile);
    }
}
