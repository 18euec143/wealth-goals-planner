package com.anmol.ProfileManagement.DTO;

public enum InvestmentCategory {
	SAVINGS_ACCOUNT,
	FIXED_ACCOUNT,
	RECURRING_DEPOSIT,
	PPF,
	EPF,
	NSC,
	SCSS,
	MUTUAL_FUNDS,
	GOLD_SAVING,
	SSY,
	NPS
}
