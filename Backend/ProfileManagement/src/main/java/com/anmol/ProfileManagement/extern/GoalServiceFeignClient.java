package com.anmol.ProfileManagement.extern;

import com.anmol.ProfileManagement.DTO.GoalDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "GOAL-SERVICE")
public interface GoalServiceFeignClient {
    @GetMapping(path = "/goal/getGoalsOfUser/{userId}")
    public List<GoalDto> getGoalsOfUser(@PathVariable Long userId);
}
