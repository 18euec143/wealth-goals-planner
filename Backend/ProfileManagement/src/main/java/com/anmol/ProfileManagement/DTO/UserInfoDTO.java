package com.anmol.ProfileManagement.DTO;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserInfoDTO {
    Long id;
    String email;

    String name;

    String userType;

    String dob;

    String address;

    String phone;

    String incomeSource;

    Double monthlySalary;

    String employmentType;
}
