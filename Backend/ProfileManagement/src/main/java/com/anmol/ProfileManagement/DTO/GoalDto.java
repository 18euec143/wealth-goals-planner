package com.anmol.ProfileManagement.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoalDto {
	
	private Long goalId;
	private String goalTitle;
	private Long goalCreatedBy; //userId
	private String goalDescription;
	private int goalPriority;
	private String goalStatus;
	private double goalAmount;
	private Date goalStartDate;
	private Date goalEndDate;
	private double goalProgress;
	private SubordinateDTO subordinate;
	private int riskFactor;

}
