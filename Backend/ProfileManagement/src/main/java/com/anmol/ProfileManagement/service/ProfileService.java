package com.anmol.ProfileManagement.service;

import com.anmol.ProfileManagement.DTO.*;
import com.anmol.ProfileManagement.extern.*;
import com.anmol.ProfileManagement.model.Profile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProfileService {

    @Autowired
    UserServiceFeignClient userServiceFeignClient;

    @Autowired
    SubordinateServiceFeignClient subordinateServiceFeignClient;

    @Autowired
    GoalServiceFeignClient goalServiceFeignClient;

    @Autowired
    ExpenseServiceFeignClient expenseServiceFeignClient;

    @Autowired
    InvestmentServiceFeignClient investmentServiceFeignClient;

    public Profile getProfileByUserId(Long endUserId){
        Profile profile = new Profile();
        ResponseEntity<UserInfoDTO> endUserInfoById = userServiceFeignClient.getEndUserInfoById(endUserId);
        ResponseEntity<List<SubordinateDTO>> allSubordinateByUserId = subordinateServiceFeignClient.getAllSubordinateByUserId(endUserId);
        List<GoalDto> goalsOfUser = goalServiceFeignClient.getGoalsOfUser(endUserId);

        ResponseEntity<List<ExpenseDTO>> byUserId = expenseServiceFeignClient.getByUserId(endUserId);
        ResponseEntity<List<InvestmentDTO>> insmtByUser = investmentServiceFeignClient.getInsmtByUser(endUserId);

        if(byUserId.getStatusCode().is2xxSuccessful()){
            profile.setExpenses(byUserId.getBody());
        }else{
            profile.setExpenses(List.of());
        }

        if(insmtByUser.getStatusCode().is2xxSuccessful()){
            profile.setInvestments(insmtByUser.getBody());
        }else{
            profile.setInvestments(List.of());
        }

        if(endUserInfoById.getStatusCode().is2xxSuccessful()){
            profile.setUserInfo(endUserInfoById.getBody());
        }else{
            profile.setUserInfo(null);
        }

        if(allSubordinateByUserId.getStatusCode().is2xxSuccessful()){
            profile.setSubordinates(allSubordinateByUserId.getBody());
        }else{
            profile.setSubordinates(List.of());
        }

        if(goalsOfUser != null){
            profile.setGoals(goalsOfUser);
        }else{
            profile.setGoals(List.of());
        }

        return  profile;
    }

}
