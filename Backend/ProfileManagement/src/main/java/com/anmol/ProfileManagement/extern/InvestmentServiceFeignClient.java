package com.anmol.ProfileManagement.extern;

import com.anmol.ProfileManagement.DTO.InvestmentDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "INVESTMENT-SERVICE")
public interface InvestmentServiceFeignClient {
    @GetMapping("/investment/getByUserId/{userId}")
    public ResponseEntity<List<InvestmentDTO>> getInsmtByUser(@PathVariable long userId);
}
