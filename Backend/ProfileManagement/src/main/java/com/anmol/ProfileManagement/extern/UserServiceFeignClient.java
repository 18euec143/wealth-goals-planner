package com.anmol.ProfileManagement.extern;

import com.anmol.ProfileManagement.DTO.UserInfoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "USER-SERVICE")
public interface UserServiceFeignClient {

    @GetMapping("/customer/info/{id}")
    ResponseEntity<UserInfoDTO> getEndUserInfoById(@PathVariable("id") Long endUserId);

}
