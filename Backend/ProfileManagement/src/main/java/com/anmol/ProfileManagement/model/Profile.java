package com.anmol.ProfileManagement.model;

import com.anmol.ProfileManagement.DTO.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Profile {

    UserInfoDTO userInfo;

    List<GoalDto> goals;

    List<ExpenseDTO> expenses;

    List<InvestmentDTO> investments;

    List<SubordinateDTO> subordinates;

}
