package com.anmol.ProfileManagement.extern;

import com.anmol.ProfileManagement.DTO.SubordinateDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "SUBORDINATE-SERVICE")
public interface SubordinateServiceFeignClient {
    @GetMapping("/subordinate/user/{id}")
    ResponseEntity<List<SubordinateDTO>> getAllSubordinateByUserId(@PathVariable("id") Long id);
}
