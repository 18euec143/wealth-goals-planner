package com.anmol.UserService.DTO;

import com.anmol.UserService.model.UserType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserInfoDTO {
    Long id;
    String email;

    String name;

    UserType userType;

    String dob;

    String address;

    String phone;

    String incomeSource;

    Double monthlySalary;

    String employmentType;
}
