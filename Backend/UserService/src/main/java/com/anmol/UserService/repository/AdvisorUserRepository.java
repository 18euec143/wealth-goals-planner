package com.anmol.UserService.repository;

import com.anmol.UserService.model.Advisor;
import com.anmol.UserService.model.EndUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdvisorUserRepository extends JpaRepository<Advisor,Long> {
}
