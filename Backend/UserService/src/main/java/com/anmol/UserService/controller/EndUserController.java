package com.anmol.UserService.controller;

import com.anmol.UserService.DTO.AddAdvisorDTO;
import com.anmol.UserService.DTO.UserInfoDTO;
import com.anmol.UserService.model.AuthRequest;
import com.anmol.UserService.model.AuthResponse;
import com.anmol.UserService.model.EndUser;
import com.anmol.UserService.model.UserType;
import com.anmol.UserService.service.EndUserService;
import com.anmol.UserService.service.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/customer")
public class EndUserController {
    @Autowired
    EndUserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtil jwtUtil;

    @PostMapping("/signup")
    public ResponseEntity<? extends  Object> create(@RequestBody EndUser endUser) {
        if(!endUser.getUserType().equals(UserType.ADVISOR)) {
            EndUser registeredUser = userService.save(endUser);

            String accessToken = jwtUtil.generate(registeredUser.getId(), registeredUser.getUserType(), "ACCESS");
            String refreshToken = jwtUtil.generate(registeredUser.getId(), registeredUser.getUserType(), "REFRESH");

            AuthResponse response = new AuthResponse(accessToken,refreshToken);

            return ResponseEntity.ok(response);
        }else{
            return ResponseEntity.badRequest().body("Advisor User Type is not valid for Customer User");
        }
    }

    @PostMapping("/signin")
    public ResponseEntity<? extends  Object> signin(@RequestBody AuthRequest request)  {
            EndUser registeredUser = userService.getByEmailId(request.getEmail());
                if(!passwordEncoder.matches(request.getPassword(), registeredUser.getPassword())){
                return ResponseEntity.badRequest().body("Bad Credentials");
            }
            String accessToken = jwtUtil.generate(registeredUser.getId(), registeredUser.getUserType(), "ACCESS");
            String refreshToken = jwtUtil.generate(registeredUser.getId(), registeredUser.getUserType(), "REFRESH");

            AuthResponse response = new AuthResponse(accessToken,refreshToken);

            return ResponseEntity.ok(response);
    }

    @PostMapping("/add-advisor")
    public void setAdvisorForEndUser(@RequestBody AddAdvisorDTO dto){
        userService.setAdvisor(dto.getEndUserId() ,dto.getAdvisorId());
    }

    @GetMapping("/info/{id}")
    public ResponseEntity<UserInfoDTO> getEndUserInfoById(@PathVariable("id") Long endUserId){
        UserInfoDTO endUserInfoById = userService.getEndUserInfoById(endUserId);
        return ResponseEntity.ok(endUserInfoById);
    }


}
