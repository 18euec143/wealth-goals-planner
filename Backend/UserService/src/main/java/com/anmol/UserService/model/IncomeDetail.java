package com.anmol.UserService.model;

import jakarta.persistence.Embeddable;
import lombok.Data;

@Embeddable
@Data
public class IncomeDetail {
    String incomeSource;

    Double monthlySalary;

    String employmentType;
}
