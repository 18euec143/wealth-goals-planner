package com.anmol.UserService.service;

import com.anmol.UserService.DTO.UserInfoDTO;
import com.anmol.UserService.exception.UserNotFoundException;
import com.anmol.UserService.model.EndUser;
import com.anmol.UserService.model.User;

public interface EndUserService {

    EndUser save(EndUser endUser);

    void setAdvisor(Long endUserId,Long advisorId);

    EndUser getByEmailId(String endUserEmailId) throws UserNotFoundException;

    UserInfoDTO getEndUserInfoById(Long endUserId) throws UserNotFoundException;
}
