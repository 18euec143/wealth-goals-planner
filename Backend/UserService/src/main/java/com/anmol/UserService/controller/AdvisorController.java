package com.anmol.UserService.controller;

import com.anmol.UserService.model.Advisor;
import com.anmol.UserService.model.EndUser;
import com.anmol.UserService.service.AdvisorUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/advisor")
public class AdvisorController {

    @Autowired
    AdvisorUserService userService;

    @PostMapping
    public ResponseEntity<Advisor> create(@RequestBody Advisor advisor){
        Advisor savedUser = userService.save(advisor);
        return  ResponseEntity.ok(savedUser);
    }

    @GetMapping("{id}/end-users")
    public  ResponseEntity<List<EndUser>> getAllEndUserById(@PathVariable("id") Long id){
        List<EndUser> allEndUser = userService.getAllEndUser(id);
        return ResponseEntity.ok(allEndUser);
    }

}
