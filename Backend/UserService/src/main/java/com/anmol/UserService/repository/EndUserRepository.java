package com.anmol.UserService.repository;

import com.anmol.UserService.model.EndUser;
import com.anmol.UserService.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EndUserRepository extends JpaRepository<EndUser,Long> {
    EndUser getByEmail(String email);
}
