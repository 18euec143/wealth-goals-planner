package com.anmol.UserService.model;


import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.PrimaryKeyJoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@PrimaryKeyJoinColumn(name = "advisor_id")
public class Advisor extends User {

    String name;

    UserType userType = UserType.ADVISOR;

    @OneToMany(mappedBy = "advisor",cascade = CascadeType.ALL)
    List<EndUser> endUsers = List.of();

    public void addEndUser(EndUser endUser){
        endUsers.add(endUser);
    }
}
