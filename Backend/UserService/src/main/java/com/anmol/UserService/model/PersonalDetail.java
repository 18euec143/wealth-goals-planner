package com.anmol.UserService.model;

import jakarta.persistence.Embeddable;
import lombok.Data;

@Embeddable
@Data
public class PersonalDetail {

    String name;

    String dob;

    String address;

    String phone;

}
