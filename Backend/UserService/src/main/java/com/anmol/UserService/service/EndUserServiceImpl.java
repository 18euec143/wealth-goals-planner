package com.anmol.UserService.service;

import com.anmol.UserService.DTO.UserInfoDTO;
import com.anmol.UserService.exception.UserNotFoundException;
import com.anmol.UserService.model.Advisor;
import com.anmol.UserService.model.EndUser;
import com.anmol.UserService.repository.AdvisorUserRepository;
import com.anmol.UserService.repository.EndUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EndUserServiceImpl implements  EndUserService{
    @Autowired
    EndUserRepository endUserRepository;

    @Autowired
    AdvisorUserRepository advisorUserRepository;

    @Autowired
    PasswordEncoder passwordEncoder;


    @Override
    public EndUser save(EndUser endUser) {
        endUser.setPassword(passwordEncoder.encode(endUser.getPassword()));
        return endUserRepository.save(endUser);
    }

    @Override
    public void setAdvisor(Long endUserId, Long advisorId) {
        Optional<EndUser> byId1 = endUserRepository.findById(endUserId);
        Optional<Advisor> byId = advisorUserRepository.findById(advisorId);
        if(byId.isPresent() && byId1.isPresent()){
            EndUser endUser = byId1.get();
            Advisor advisor = byId.get();
            endUser.setAdvisor(advisor);
            advisor.addEndUser(endUser);

            advisorUserRepository.save(advisor);
        }

    }

    @Override
    public EndUser getByEmailId(String endUserEmailId) throws UserNotFoundException {
        EndUser byEmail = endUserRepository.getByEmail(endUserEmailId);
        if(byEmail != null){
            return  byEmail;
        }else{
            throw new UserNotFoundException(endUserEmailId);
        }
    }

    @Override
    public UserInfoDTO getEndUserInfoById(Long endUserId) throws UserNotFoundException {
        Optional<EndUser> byId = endUserRepository.findById(endUserId);
        if(byId.isPresent()){
            EndUser user = byId.get();
            UserInfoDTO userInfoDTO = UserInfoDTO.builder()
                    .id(user.getId())
                    .dob(user.getPersonalDetail().getDob())
                    .userType(user.getUserType())
                    .email(user.getEmail())
                    .name(user.getPersonalDetail().getName())
                    .phone(user.getPersonalDetail().getName())
                    .employmentType(user.getIncomeDetail().getEmploymentType())
                    .incomeSource(user.getIncomeDetail().getIncomeSource())
                    .monthlySalary(user.getIncomeDetail().getMonthlySalary())
                    .address(user.getPersonalDetail().getAddress())
                    .build();
            return  userInfoDTO;
        }else{
            throw new UserNotFoundException(endUserId.toString());
        }

    }
}
