package com.anmol.UserService.service;

import com.anmol.UserService.exception.UserNotFoundException;
import com.anmol.UserService.model.Advisor;
import com.anmol.UserService.model.EndUser;

import java.util.List;

public interface AdvisorUserService {

    Advisor save(Advisor advisor);

    List<EndUser> getAllEndUser(Long advisorId) throws UserNotFoundException;
}
