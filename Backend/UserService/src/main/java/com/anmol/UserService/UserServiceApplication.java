package com.anmol.UserService;

import com.anmol.UserService.model.Advisor;
import com.anmol.UserService.service.AdvisorUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@SpringBootApplication
public class UserServiceApplication implements CommandLineRunner {


	@Autowired
	AdvisorUserService userService;


	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Advisor advisor1 = new Advisor();
		advisor1.setId(100L);
		advisor1.setEmail("advisor1@example.com");
		advisor1.setPassword("password123");
		advisor1.setName("John Doe");
		advisor1.setEndUsers(Collections.emptyList());

		Advisor advisor2 = new Advisor();
		advisor2.setId(200L);
		advisor2.setEmail("advisor2@example.com");
		advisor2.setPassword("password456");
		advisor2.setName("Jane Anderson");
		advisor2.setEndUsers(Collections.emptyList());

		userService.save(advisor1);
		userService.save(advisor2);
	}
}
