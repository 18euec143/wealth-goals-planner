package com.anmol.UserService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@PrimaryKeyJoinColumn(name = "end_user_id")
public class EndUser extends User {
    UserType userType;

    @ManyToOne
    @JoinColumn(name = "advisor_id")
    @JsonIgnore
    Advisor advisor;

    @Embedded
    PersonalDetail personalDetail;

    @Embedded
    IncomeDetail incomeDetail;
}
