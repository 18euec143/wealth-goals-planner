package com.anmol.UserService.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class AddAdvisorDTO {
    Long endUserId;
    Long advisorId;
}
