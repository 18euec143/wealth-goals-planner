package com.anmol.UserService.service;

import com.anmol.UserService.exception.UserNotFoundException;
import com.anmol.UserService.model.Advisor;
import com.anmol.UserService.model.EndUser;
import com.anmol.UserService.model.User;
import com.anmol.UserService.repository.AdvisorUserRepository;
import com.anmol.UserService.repository.EndUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AdvisorUserServiceImpl implements AdvisorUserService {
    @Autowired
    AdvisorUserRepository advisorUserRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Advisor save(Advisor advisor) {
        advisor.setPassword(passwordEncoder.encode(advisor.getPassword()));
        return advisorUserRepository.save(advisor);
    }

    @Override
    public List<EndUser> getAllEndUser(Long advisorId) {
        Optional<Advisor> byId = advisorUserRepository.findById(advisorId);
        if(byId.isPresent()){
            Advisor advisor = byId.get();
            return advisor.getEndUsers();
        }else{
            throw new UserNotFoundException(advisorId.toString());
        }
    }
}
