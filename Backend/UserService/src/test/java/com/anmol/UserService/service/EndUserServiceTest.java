package com.anmol.UserService.service;

import com.anmol.UserService.exception.UserNotFoundException;
import com.anmol.UserService.model.EndUser;
import com.anmol.UserService.model.UserType;
import com.anmol.UserService.repository.EndUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class EndUserServiceTest {
    @Autowired
    private EndUserService endUserService;


    @Mock
    private EndUserRepository endUserRepository;

    EndUser dummyEndUser = new EndUser();

    @BeforeEach
    public void setup(){
        dummyEndUser.setEmail("dummy@example.com");
        dummyEndUser.setPassword("dummyPassword");
        dummyEndUser.setUserType(UserType.INTERNAL_USER);
        dummyEndUser.setPersonalDetail(null);
        dummyEndUser.setIncomeDetail(null);
    }

    @Test
    public void testEndUserSave(){
        Mockito.when(endUserService.save(dummyEndUser)).thenReturn(dummyEndUser);
        EndUser save = endUserService.save(dummyEndUser);
        assertEquals("dummy@example.com",save.getEmail());
    }

    @Test
    public void testGetByEmailId(String endUserEmailId){
        Mockito.when(endUserService.getByEmailId("dummy@example.com")).thenReturn(dummyEndUser);
        EndUser byEmailId = endUserService.getByEmailId("dummy@example.com");
        System.out.println(byEmailId);
        assertEquals(byEmailId.getEmail(),dummyEndUser.getEmail());
    }
    @Test
    public void testGetByEmailThrowException(String endUserEmailId){
        Mockito.when(endUserService.getByEmailId("abc")).thenThrow(new UserNotFoundException("abc"));
        assertThrows(UserNotFoundException.class,() -> {
            EndUser byEmailId = endUserService.getByEmailId("abc");
        });
    }

}

