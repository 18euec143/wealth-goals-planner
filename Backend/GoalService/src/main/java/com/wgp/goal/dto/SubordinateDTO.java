package com.wgp.goal.dto;

import lombok.Data;

@Data
public class SubordinateDTO {
    private long id;

    private Long userId;

    private String name;
    private double age;

    private String relation;
}
