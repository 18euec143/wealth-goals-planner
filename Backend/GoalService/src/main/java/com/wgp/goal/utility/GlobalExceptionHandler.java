package com.wgp.goal.utility;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	//general exception
	@ExceptionHandler
	public String exceptionalHandler(Exception e) {
		return "Problem occured : "+e.getMessage();
	}

}
