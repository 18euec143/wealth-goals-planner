package com.wgp.goal.controller;

import java.util.List;

import com.wgp.goal.dto.StatusDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wgp.goal.dto.GoalDto;
import com.wgp.goal.entity.Goal;
import com.wgp.goal.service.GoalService;

@RestController
@RequestMapping(path = "/goal")
public class GoalController {
	
	private final Logger log = Logger.getLogger(GoalController.class);
	
	@Autowired
	private GoalService goalService;
	
	@PostMapping(path = "/createGoal")
	public GoalDto createGoal(@RequestBody Goal goal) {
		log.info("Goal created by user, userId : "+goal.getGoalCreatedBy());
		return goalService.createGoal(goal);
	}
	
	@GetMapping(path = "/getGoalById/{goalId}")
	public GoalDto getGoalById(@PathVariable Long goalId) {
		log.info("Goal fetched!");
		return goalService.getGoalById(goalId);
	}
	
	@PutMapping(path = "/updateGoal")
	public GoalDto updateGoalById(@RequestBody Goal goal) {
		log.info("Goal updated by user, userId : "+goal.getGoalCreatedBy());
		return goalService.updateGoalById(goal);
	}
	
	@DeleteMapping(path = "/deleteGoalById/{goalId}")
	public void deleteGoalById(Long goalId) {
		goalService.deleteGoalById(goalId);
		log.info("Goal deleted!, goalId : "+goalId);
	}
	
	@GetMapping(path = "/getAllGoals")
	public List<GoalDto> getAllGoals(){
		log.info("All goals fetched!");
		return goalService.getAllGoals();
	}
	
	@PutMapping(path = "/updateGoalStatusById/{goalId}")
	public GoalDto updateGoalStatusById(@RequestBody StatusDTO dto, @PathVariable Long goalId ){
		log.info("Updated goal status");
		return goalService.updateGoalStatusByGoalId(dto.getStatus(),goalId);
	}
	
	@GetMapping(path = "/getGoalsOfUser/{userId}")
	public List<GoalDto> getGoalsOfUser(@PathVariable Long userId){
		log.info("All goals of user fetched!");
		return goalService.getGoalsOfUser(userId);
	}

}
