package com.wgp.goal.dto;

import com.wgp.goal.entity.GoalStatus;
import lombok.Data;

@Data
public class StatusDTO {
    GoalStatus status;
}
