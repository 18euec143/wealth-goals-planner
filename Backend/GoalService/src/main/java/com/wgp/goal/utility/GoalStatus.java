package com.wgp.goal.utility;

public enum GoalStatus {
	APPROVED,
	REJECTED,
	PENDING,
	ABANDONED
}
