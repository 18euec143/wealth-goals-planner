package com.wgp.goal.entity;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Goal {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long goalId;
	private String goalTitle;
	private Long goalCreatedBy; //userId
	private String goalDescription;
	private int goalPriority;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private GoalStatus goalStatus;
	private double goalAmount;
	private Date goalStartDate;
	private Date goalEndDate;
	private double goalProgress;
	private Long subordinateId;
	private int riskFactor;
	
}
