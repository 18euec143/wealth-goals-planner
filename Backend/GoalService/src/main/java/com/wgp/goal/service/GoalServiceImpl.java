package com.wgp.goal.service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import com.wgp.goal.dto.StatusDTO;
import com.wgp.goal.dto.SubordinateDTO;
import com.wgp.goal.entity.GoalStatus;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wgp.goal.clients.SubordinateFeignClient;
import com.wgp.goal.dto.GoalDto;
import com.wgp.goal.entity.Goal;
import com.wgp.goal.repository.GoalRepository;

import jakarta.transaction.Transactional;
@Service
public class GoalServiceImpl implements GoalService {
	
	@Autowired
	private GoalRepository goalRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private SubordinateFeignClient subordinateFeignClient;

	@Override
	public GoalDto createGoal(Goal goal) {
		goal.setGoalStatus(GoalStatus.PENDING);
		return goalToGoalDto(goalRepository.save(goal));
		
	}

	@Override
	public GoalDto getGoalById(Long goalId) throws NoSuchElementException {
		return goalToGoalDto(goalRepository.findById(goalId).get());
	}

	@Override
	public GoalDto updateGoalById(Goal goal) {
		return goalToGoalDto(goalRepository.save(goal));
	}

	@Override
	public void deleteGoalById(Long goalId) {
		goalRepository.deleteById(goalId);
	}

	@Override
	public List<GoalDto> getAllGoals() {
		List<Goal> listOfGoalsResponse = goalRepository.findAll();
		List<GoalDto> listOfGoalsDto = new ArrayList<>();
		listOfGoalsResponse.stream().forEach(goalResponse->{
			listOfGoalsDto.add(goalToGoalDto(goalResponse));
		});
		return listOfGoalsDto;
	}

	@Override
	@Transactional
	public GoalDto updateGoalStatusByGoalId(GoalStatus goalStatus, Long goalId) {
		
		goalRepository.updateGoalStatusByGoalId(goalStatus, goalId);
		return getGoalById(goalId);
	}

	@Override
	public List<GoalDto> getGoalsOfUser(Long userId) {
		List<Goal> listOfGoals = goalRepository.findByGoalCreatedBy(userId);
		List<GoalDto> listOfGoalDtos = new ArrayList<>();
		listOfGoals.stream().forEach(goal->{
			listOfGoalDtos.add(goalToGoalDto(goal));
		});
		return listOfGoalDtos;
	}
	
	private GoalDto goalToGoalDto(Goal goal) {
		SubordinateDTO subordinateById = subordinateFeignClient.getSubordinateById(goal.getSubordinateId());
		GoalDto goalDto = modelMapper.map(goal, GoalDto.class);
		goalDto.setSubordinate(subordinateById);
		return goalDto;
	}

}
