package com.wgp.goal.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GoalDto {
	
	private Long goalId;
	private String goalTitle;
	private Long goalCreatedBy; //userId
	private String goalDescription;
	private int goalPriority;
	private String goalStatus;
	private double goalAmount;
	private Date goalStartDate;
	private Date goalEndDate;
	private double goalProgress;
	private SubordinateDTO subordinate;
	private int riskFactor;

}
