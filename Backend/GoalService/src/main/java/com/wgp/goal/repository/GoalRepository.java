package com.wgp.goal.repository;

import java.util.List;

import com.wgp.goal.dto.GoalDto;
import com.wgp.goal.dto.StatusDTO;
import com.wgp.goal.entity.GoalStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.wgp.goal.entity.Goal;

@Repository
public interface GoalRepository extends JpaRepository<Goal, Long> {
	
	@Modifying
	@Query("Update Goal SET goalStatus = :goalStatus WHERE goalId = :goalId ")
	public int updateGoalStatusByGoalId(@Param("goalStatus") GoalStatus goalStatus, @Param("goalId") Long goalId);
	
	public List<Goal> findByGoalCreatedBy(Long userId);

}
