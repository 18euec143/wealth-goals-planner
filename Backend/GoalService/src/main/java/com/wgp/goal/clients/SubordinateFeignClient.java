package com.wgp.goal.clients;

import com.wgp.goal.dto.SubordinateDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "SUBORDINATE-SERVICE")
public interface SubordinateFeignClient {
	
	@GetMapping(path = "/subordinate/{subordinateId}" )
	public SubordinateDTO getSubordinateById(@PathVariable Long subordinateId);

}
