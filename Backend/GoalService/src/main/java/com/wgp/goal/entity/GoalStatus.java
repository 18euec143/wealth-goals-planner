package com.wgp.goal.entity;

public enum GoalStatus {
    PENDING,APPROVED,REJECTED
}
