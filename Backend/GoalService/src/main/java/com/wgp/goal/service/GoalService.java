package com.wgp.goal.service;

import java.util.List;

import com.wgp.goal.dto.GoalDto;
import com.wgp.goal.dto.StatusDTO;
import com.wgp.goal.entity.Goal;
import com.wgp.goal.entity.GoalStatus;

public interface GoalService {
	
	public GoalDto createGoal(Goal goal);
	public GoalDto getGoalById(Long goalId);
	public GoalDto updateGoalById(Goal goal);
	public void deleteGoalById(Long goalId);
	public List<GoalDto> getAllGoals();
	public GoalDto updateGoalStatusByGoalId(GoalStatus goalStatus, Long goalId);
	public List<GoalDto> getGoalsOfUser(Long userId);

}
