package com.subordinate.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.subordinate.entity.Subordinate;
import com.subordinate.repository.SubordinateRepository;
import com.subordinate.service.SubordinateService;

@Service
public class SubordinateServiceImpl implements SubordinateService {

	@Autowired
	public SubordinateRepository subordinateRepository;

	@Override
	public Subordinate createSubordinate(Subordinate subordinate) {
		return subordinateRepository.save(subordinate);
	}
	
	public Optional<Subordinate> getSubordinateById(long id) {
        return subordinateRepository.findById(id);
    }
	
	public Subordinate updateSubordinate(long id, Subordinate updatedSubordinate) {
        Optional<Subordinate> existingSubordinate = subordinateRepository.findById(id);
        if (existingSubordinate.isPresent()) {
        	Subordinate subordinateToUpdate = existingSubordinate.get();
        	subordinateToUpdate.setName(updatedSubordinate.getName());
        	subordinateToUpdate.setAge(updatedSubordinate.getAge());
        	subordinateToUpdate.setRelation(updatedSubordinate.getRelation());
        	subordinateRepository.save(subordinateToUpdate);
        }
		return updatedSubordinate; 
    }
	
	public boolean deleteSubordinate(long id) {
        if (subordinateRepository.existsById(id)) {
        	subordinateRepository.deleteById(id);
            return true;
        }
        return false;
    }

	@Override
	public List<Subordinate> getSubordinatesByUserId(Long userId) {
		return subordinateRepository.getAllSubordinateByUserId(userId);
	}

}
