package com.subordinate.service;

import java.util.List;
import java.util.Optional;

import com.subordinate.entity.Subordinate;

public interface SubordinateService {
	
	public Subordinate createSubordinate(Subordinate subordinate);
	public Optional<Subordinate> getSubordinateById(long id);
	public Subordinate updateSubordinate(long id ,Subordinate updatedSubordinate);
	public boolean deleteSubordinate(long id);

	List<Subordinate> getSubordinatesByUserId(Long userId);

}
