package com.subordinate.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.subordinate.entity.Subordinate;
import com.subordinate.service.SubordinateService;

@Controller
@RequestMapping("/subordinate")
public class SubordinateController {

	@Autowired
	public SubordinateService subordinateService;

	@GetMapping("/user/{id}")
	public ResponseEntity<List<Subordinate>> getAllSubordinateByUserId(@PathVariable("id") Long id){
		List<Subordinate> subordinatesByUserId = subordinateService.getSubordinatesByUserId(id);
		return ResponseEntity.ok(subordinatesByUserId);
	}

	@PostMapping
	public ResponseEntity<Subordinate> createSubordinate(@RequestBody Subordinate subordinate) {
		Subordinate newSubordinate = subordinateService.createSubordinate(subordinate);
		return new ResponseEntity<>(newSubordinate, HttpStatus.CREATED);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Subordinate> getSubordinateById(@PathVariable Long id) {
		Optional<Subordinate> subordinateOptional = subordinateService.getSubordinateById(id);
		if (subordinateOptional.isPresent()) {
			Subordinate subordinate = subordinateOptional.get();
			return ResponseEntity.ok(subordinate);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<Subordinate> updateSubordinate(@PathVariable long id, @RequestBody Subordinate subordinate) {
		Subordinate updated = subordinateService.updateSubordinate(id, subordinate);
		if (updated != null) {
			return ResponseEntity.status(HttpStatus.OK).build();
		} else {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
	}

	@DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSubordinate(@PathVariable long id) {
        boolean deleted = subordinateService.deleteSubordinate(id);
        if (deleted) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
