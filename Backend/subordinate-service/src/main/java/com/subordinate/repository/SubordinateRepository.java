package com.subordinate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.subordinate.entity.Subordinate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubordinateRepository extends JpaRepository<Subordinate, Long> {

    @Query("select s from Subordinate s where s.userId = :userId")
    List<Subordinate> getAllSubordinateByUserId(@Param("userId") Long userId);

}
