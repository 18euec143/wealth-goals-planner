package com.planner.service;

import java.util.List;

import com.planner.entity.InvestmentDTO;
import com.planner.entity.Investments;

public interface InvestmentService {

	String addInstmt(Investments inst);

	InvestmentDTO getInsmt(long id);

	List<InvestmentDTO> getInsmtByUser(long id);

	InvestmentDTO updateById(long id, Investments inst);

	String deleteById(long id);
}
