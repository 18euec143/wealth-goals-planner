package com.planner.service.impl;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.planner.entity.InvestmentDTO;
import com.planner.entity.Investments;
import com.planner.repository.InvestmentRepository;
import com.planner.service.InvestmentService;

@Service
public class InvestmentServiceImpl implements InvestmentService{
	
	@Autowired
	ModelMapper modelMapper;
	
	@Autowired
	private InvestmentRepository investmentRepository;

	@Override
	public String addInstmt(Investments inst) {
		investmentRepository.save(inst);
		return "Added";
	}

	@Override
	public InvestmentDTO getInsmt(long id) {
		Optional<Investments> invst = investmentRepository.findById(id);
		if(invst.isPresent()) {
			return modelMapper.map(invst.get(), InvestmentDTO.class);
		}
		return null; 
	}

	@Override
	public List<InvestmentDTO> getInsmtByUser(long userId) {
		List<Investments> instList = investmentRepository.findAllByUserId(userId);
		List<InvestmentDTO> investmentDTOS = instList.stream().map(el -> modelMapper.map(el, InvestmentDTO.class)).toList();
		return investmentDTOS;
	}

	@Override
	public InvestmentDTO updateById(long id, Investments inst) {
		Optional<Investments> invst = investmentRepository.findById(id);
		if(invst.isPresent()) {
			Investments existingInst = invst.get();
			existingInst.setCategory(inst.getCategory());
			existingInst.setDescription(inst.getDescription());
			investmentRepository.save(existingInst);
		}
		
		return modelMapper.map(inst, InvestmentDTO.class);
	}

	@Override
	public String deleteById(long id) {
		Optional<Investments> invst = investmentRepository.findById(id);
		if(invst.isPresent()) {
			investmentRepository.deleteById(id);
			return "Successfully deleted";
		}
		return "Failed to delete";
	}
	
	
	


}
