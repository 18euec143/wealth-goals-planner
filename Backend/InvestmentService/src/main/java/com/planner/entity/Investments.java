package com.planner.entity;

import com.planner.enums.InvestmentCategory;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Investments {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long instId;
	private int priority;
	private InvestmentCategory category;
	private String description;
	private long userId;
}
