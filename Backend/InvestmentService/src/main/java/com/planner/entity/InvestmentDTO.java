package com.planner.entity;

import com.planner.enums.InvestmentCategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvestmentDTO {

	private long instId;
	private int priority;
	private InvestmentCategory category;
	private String description;
	private long userId;
}
