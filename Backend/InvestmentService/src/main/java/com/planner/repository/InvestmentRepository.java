package com.planner.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.planner.entity.Investments;

public interface InvestmentRepository extends JpaRepository<Investments, Long>{

	Investments findByUserId(long userId);

	List<Investments> findAllByUserId(long userId);

}
