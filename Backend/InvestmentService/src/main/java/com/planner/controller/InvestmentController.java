package com.planner.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.planner.entity.InvestmentDTO;
import com.planner.entity.Investments;
import com.planner.exception.ObjectNotFoundException;
import com.planner.service.InvestmentService;

@RestController
@RequestMapping("/investment")
public class InvestmentController {
	@Autowired
	private InvestmentService investmentService;
	
	@PostMapping("/addInst")
	public ResponseEntity<String> addInsmt(@RequestBody Investments inst){
		investmentService.addInstmt(inst);
		return ResponseEntity.status(HttpStatus.CREATED).body("Successfully added");
	}
	
	@GetMapping("/getById/{id}")
	public ResponseEntity<InvestmentDTO> getInsmt(@PathVariable long id) throws ObjectNotFoundException{
		InvestmentDTO inst = investmentService.getInsmt(id);
		if(inst!=null) {
			return ResponseEntity.ok(inst);
		}
		throw new ObjectNotFoundException("Object with given id is not present!");
	}
	
	@GetMapping("/getByUserId/{userId}")
	public ResponseEntity<List<InvestmentDTO>> getInsmtByUser(@PathVariable long userId){
		return ResponseEntity.ok(investmentService.getInsmtByUser(userId));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<InvestmentDTO> updateById(@PathVariable long id, @RequestBody Investments inst){
		return ResponseEntity.ok(investmentService.updateById(id, inst));
	}
	
	@DeleteMapping("/{id}")
	public String deleteById(@PathVariable long id) {
		return investmentService.deleteById(id);
	}
	
}
